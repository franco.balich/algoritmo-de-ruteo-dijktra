﻿using System.Collections.Generic;
using System.Linq;

namespace Algoritmo_de_ruteo
{
    public class Gestor_Dijktra
    {
        private static Grafo miGrafo;
        public static Grafo GrafoPrincipal { get => miGrafo; set => miGrafo = value; }

        public static string CaminoMasCorto(int inicio, int final)
        {
            decimal distancia;
            int n;
            string cadena = "";
            int actual = 0;
            int columna;

            int[,] tabla = new int[miGrafo.CantidadNodos, 3];

            for (n = 0; n < miGrafo.CantidadNodos; n++)
            {
                tabla[n, 0] = 0;
                tabla[n, 1] = int.MaxValue;
                tabla[n, 2] = 0;
            }
            tabla[inicio, 1] = 0;

            do
            {
                tabla[actual, 0] = 1;

                for (columna = 0; columna < miGrafo.CantidadNodos; columna++)
                {
                    if (miGrafo.ObtenAyacencia(actual, columna) != 0)
                    {
                        distancia = miGrafo.ObtenAyacencia(actual, columna) + tabla[actual, 1];
                        if (distancia < tabla[columna, 1])
                        {
                            tabla[columna, 1] = (int)distancia;
                            tabla[columna, 2] = actual;

                        }
                    }
                }
                int indiceMenor = -1;
                int distanciaMenor = int.MaxValue;

                for (int x = 0; x < miGrafo.CantidadNodos; x++)
                {
                    if (tabla[x, 1] < distanciaMenor && tabla[x, 0] == 0)
                    {
                        indiceMenor = x;
                        distanciaMenor = tabla[x, 0];
                    }
                }
                actual = indiceMenor;
            } while (actual != -1);

            List<int> ruta = new List<int>();
            int nodo = final;

            while (nodo != inicio)
            {
                ruta.Add(nodo);
                nodo = tabla[nodo, 2];
            }
            ruta.Add(inicio);
            ruta.Reverse();
            foreach (int posicion in ruta)
            {
                cadena += $"{posicion}->";
            }
            cadena += $"\r\n";
            foreach (int posicion in ruta)
            {
                if (posicion != ruta.Last())
                {
                    Nodo nodoPadre = Grafo.BuscarNodo(posicion);
                    Nodo nodoHijo = Grafo.BuscarNodo(ruta.ElementAt(ruta.IndexOf(posicion) + 1));
                    Arista arista = nodoPadre.BuscarArista(nodoHijo);
                    if (arista != null)
                    {
                        cadena += $"{arista.Peso}->";
                    }
                }
                else
                {
                    cadena += $"FIN";
                }
            }
            return cadena;
        }
        public static List<int> CaminoMasCortoToList(int inicio, int final)
        {
            decimal distancia;
            int n;
            string cadena = "";
            int actual = 0;
            int columna;
            int distanciaMenor;

            int[,] tabla = new int[miGrafo.CantidadNodos, 3];


            for (n = 0; n < miGrafo.CantidadNodos; n++)
            {
                tabla[n, 0] = 0;
                tabla[n, 1] = int.MaxValue;
                tabla[n, 2] = 0;
            }
            tabla[inicio, 1] = 0;
            do
            {
                tabla[actual, 0] = 1;

                for (columna = 0; columna < miGrafo.CantidadNodos; columna++)
                {
                    if (miGrafo.ObtenAyacencia(actual, columna) != 0)
                    {
                        distancia = miGrafo.ObtenAyacencia(actual, columna) + tabla[actual, 1];

                        if (distancia < tabla[columna, 1])
                        {
                            tabla[columna, 1] = (int)distancia;
                            tabla[columna, 2] = actual;

                        }
                    }
                }

                int indiceMenor = -1;
                distanciaMenor = int.MaxValue;

                for (int x = 0; x < miGrafo.CantidadNodos; x++)
                {
                    if (tabla[x, 1] < distanciaMenor && tabla[x, 0] == 0)
                    {
                        indiceMenor = x;
                        distanciaMenor = tabla[x, 0];
                    }
                }
                actual = indiceMenor;
            } while (actual != -1);

            List<int> ruta = new List<int>();
            List<int> resultado = new List<int>();
            int nodo = final;

            while (nodo != inicio)
            {
                ruta.Add(nodo);
                nodo = tabla[nodo, 2];
            }
            ruta.Add(inicio);
            ruta.Reverse();
            foreach (int posicion in ruta)
            {
                cadena += $"{posicion}->";
                resultado.Add(posicion);
            }
            cadena += $"\r\n";
            foreach (int posicion in ruta)
            {
                if (posicion != ruta.Last())
                {
                    Nodo nodoPadre = Grafo.BuscarNodo(posicion);
                    Nodo nodoHijo = Grafo.BuscarNodo(ruta.ElementAt(ruta.IndexOf(posicion) + 1));
                    Arista arista = nodoPadre.BuscarArista(nodoHijo);
                    if (arista != null)
                    {
                        cadena += $"{arista.Peso}->";
                    }
                }
                else
                {
                    cadena += $"FIN";
                }

            }
            Grafo.ResetCamino();
            for (int i = 0; i < resultado.Count; i++)
            {
                Nodo camino = Grafo.BuscarNodo(resultado.ElementAt(i));
                if (i < resultado.Count - 1)
                {
                    Nodo sig = Grafo.BuscarNodo(resultado.ElementAt(i + 1));
                    camino.SiguienteDijktra = sig;
                }
                Grafo.CargarCamino(camino);
                Grafo.DibujarCaminoCritico();
            }
            return resultado;
        }
    }
}

