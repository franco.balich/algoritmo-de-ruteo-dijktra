﻿using System.Windows.Forms;

namespace Algoritmo_de_ruteo
{
    public partial class GridPersonalizada : UserControl
    {

        public GridPersonalizada()
        {
            InitializeComponent();
            GridComponente.Width = Width;
            GridComponente.Height = Height;
        }

        public DataGridView Grid => GridComponente;

    }
}
