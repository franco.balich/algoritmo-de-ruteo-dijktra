﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;

namespace Algoritmo_de_ruteo
{
    public partial class VistaGeneral : Form
    {
        private int posY = 0;
        private int posX = 0;
        private Form activeForm = null;
        private Grafo grafo;
        private VistaPizarra pizarra;
        public VistaPizarra Pizarra { get => pizarra; set => pizarra = value; }

        public VistaGeneral()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                CargarPizarra();
                grafo = new Grafo(Pizarra);
                grafo.AgregarNodo(this);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void BtnAddNewNode_Click(object sender, EventArgs e)
        {
            try
            {
                grafo.AgregarNodo(this);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void CargarPizarra()
        {
            try
            {
                AbrirVistaPizarra();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void AbrirVistaPizarra()
        {
            Pizarra = new VistaPizarra();
            if (activeForm != null)
            {
                activeForm.Close();
            }
            activeForm = Pizarra;
            Pizarra.TopLevel = false;
            Pizarra.FormBorderStyle = FormBorderStyle.None;
            Pizarra.Dock = DockStyle.Fill;
            pnlPizarra.Controls.Add(Pizarra);
            pnlPizarra.Tag = Pizarra;
            Pizarra.BringToFront();
            Pizarra.Show();
        }

        private void PcbClosed_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void PcbMin_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void Panel3_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left)
            {
                posX = e.X;
                posY = e.Y;
            }
            else
            {
                Left += (e.X - posX);
                Top += (e.Y - posY);
            }
        }

        private void BtnAddArista_Click(object sender, EventArgs e)
        {
            pizarra.AddArista();
        }

        private void BtnDijkstra_Click(object sender, EventArgs e)
        {
            try
            {

                //lblprueba.Text = grafo.MuestraAdyacencia();
                int a = int.Parse(Interaction.InputBox("Ingrese el nombre de la computadora del inicio del camino:"));
                if (a <= 20 && a > 0)
                {
                    int b = int.Parse(Interaction.InputBox("Ingrese el nombre de la computadora del final del camino:"));
                    if (b <= 20 && b > 0)
                    {
                        //lblAlgoritmo.Text = Gestor_Dijktra.CaminoMasCorto(a, b);
                        List<int> vista = Gestor_Dijktra.CaminoMasCortoToList(a, b);

                        CargarDatos(vista);
                        Graficador.LimpiarPantalla(Pizarra);
                        Graficador.Redibujar();
                    }
                    else
                    {
                        MessageBox.Show("El numero no se encuentra dentro del rango de computadoras aceptable.\r\n(Rango posible: 1-20).", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    MessageBox.Show("El numero no se encuentra dentro del rango de computadoras aceptable.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (OutOfMemoryException)
            {
                MessageBox.Show("No se a podido encontrar el mejor camino entre las dos computadoras indicadas", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            catch (IndexOutOfRangeException)
            {
                MessageBox.Show("El camino solicitado, no es posible debido a que alguna de las computadoras no existe.", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void CargarDatos(List<int> pList)
        {
            GridDijkstra.Grid.Columns.Clear();
            for (int i = 0; i < pList.Count; i++)
            {
                GridDijkstra.Grid.Columns.Add(i.ToString(), pList.ElementAt(i).ToString());
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                grafo.VerTablaDeEnrutamiento();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            try
            {
                string pdfPath = @"Manual de ayuda.pdf";
                /*
                using (FileStream fileStream = new FileStream(pdfPath, FileMode.Create, FileAccess.Write))
                {
                    using (BinaryWriter binaryWriter = new BinaryWriter(fileStream))
                    {
                        binaryWriter.Write(Properties.Resources.Manual_de_ayuda);
                    }
                }*/
                Process.Start(pdfPath);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
