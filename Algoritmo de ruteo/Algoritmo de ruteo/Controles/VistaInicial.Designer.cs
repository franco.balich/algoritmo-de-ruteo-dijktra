﻿namespace Algoritmo_de_ruteo
{
    partial class VistaGeneral
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VistaGeneral));
            this.btnAddNewNode = new System.Windows.Forms.Button();
            this.btnAddArista = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnDijkstra = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.lblAlgoritmo = new System.Windows.Forms.Label();
            this.lblprueba = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pcbMin = new System.Windows.Forms.PictureBox();
            this.pcbClosed = new System.Windows.Forms.PictureBox();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblSubtitulo = new System.Windows.Forms.Label();
            this.pnlPizarra = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.GridDijkstra = new Algoritmo_de_ruteo.GridPersonalizada();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcbMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbClosed)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnAddNewNode
            // 
            this.btnAddNewNode.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(147)))), ((int)(((byte)(183)))), ((int)(((byte)(191)))));
            this.btnAddNewNode.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAddNewNode.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAddNewNode.Font = new System.Drawing.Font("Nirmala UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddNewNode.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(13)))), ((int)(((byte)(13)))));
            this.btnAddNewNode.Location = new System.Drawing.Point(0, 0);
            this.btnAddNewNode.Name = "btnAddNewNode";
            this.btnAddNewNode.Size = new System.Drawing.Size(200, 46);
            this.btnAddNewNode.TabIndex = 0;
            this.btnAddNewNode.Text = "Agregar nuevo dispositivo";
            this.btnAddNewNode.UseVisualStyleBackColor = false;
            this.btnAddNewNode.Click += new System.EventHandler(this.BtnAddNewNode_Click);
            // 
            // btnAddArista
            // 
            this.btnAddArista.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(147)))), ((int)(((byte)(183)))), ((int)(((byte)(191)))));
            this.btnAddArista.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAddArista.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAddArista.Font = new System.Drawing.Font("Nirmala UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddArista.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(13)))), ((int)(((byte)(13)))));
            this.btnAddArista.Location = new System.Drawing.Point(0, 46);
            this.btnAddArista.Name = "btnAddArista";
            this.btnAddArista.Size = new System.Drawing.Size(200, 46);
            this.btnAddArista.TabIndex = 1;
            this.btnAddArista.Text = "Agregar Arista";
            this.btnAddArista.UseVisualStyleBackColor = false;
            this.btnAddArista.Click += new System.EventHandler(this.BtnAddArista_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(147)))), ((int)(((byte)(183)))), ((int)(((byte)(191)))));
            this.panel1.Controls.Add(this.btnDijkstra);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.lblAlgoritmo);
            this.panel1.Controls.Add(this.lblprueba);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.btnAddArista);
            this.panel1.Controls.Add(this.btnAddNewNode);
            this.panel1.Location = new System.Drawing.Point(0, 247);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 610);
            this.panel1.TabIndex = 2;
            // 
            // btnDijkstra
            // 
            this.btnDijkstra.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(147)))), ((int)(((byte)(183)))), ((int)(((byte)(191)))));
            this.btnDijkstra.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnDijkstra.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnDijkstra.Font = new System.Drawing.Font("Nirmala UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDijkstra.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(13)))), ((int)(((byte)(13)))));
            this.btnDijkstra.Location = new System.Drawing.Point(0, 472);
            this.btnDijkstra.Name = "btnDijkstra";
            this.btnDijkstra.Size = new System.Drawing.Size(200, 46);
            this.btnDijkstra.TabIndex = 12;
            this.btnDijkstra.Text = "Calcular Dijkstra";
            this.btnDijkstra.UseVisualStyleBackColor = false;
            this.btnDijkstra.Click += new System.EventHandler(this.BtnDijkstra_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(147)))), ((int)(((byte)(183)))), ((int)(((byte)(191)))));
            this.button2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button2.Font = new System.Drawing.Font("Nirmala UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(13)))), ((int)(((byte)(13)))));
            this.button2.Location = new System.Drawing.Point(0, 518);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(200, 46);
            this.button2.TabIndex = 11;
            this.button2.Text = "Ayuda (?)";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(147)))), ((int)(((byte)(183)))), ((int)(((byte)(191)))));
            this.button1.Dock = System.Windows.Forms.DockStyle.Top;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Font = new System.Drawing.Font("Nirmala UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(13)))), ((int)(((byte)(13)))));
            this.button1.Location = new System.Drawing.Point(0, 92);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(200, 46);
            this.button1.TabIndex = 10;
            this.button1.Text = "Ver tabla de enrutamiento";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // lblAlgoritmo
            // 
            this.lblAlgoritmo.AutoSize = true;
            this.lblAlgoritmo.ForeColor = System.Drawing.Color.White;
            this.lblAlgoritmo.Location = new System.Drawing.Point(13, 220);
            this.lblAlgoritmo.Name = "lblAlgoritmo";
            this.lblAlgoritmo.Size = new System.Drawing.Size(0, 13);
            this.lblAlgoritmo.TabIndex = 9;
            // 
            // lblprueba
            // 
            this.lblprueba.AutoSize = true;
            this.lblprueba.ForeColor = System.Drawing.Color.White;
            this.lblprueba.Location = new System.Drawing.Point(13, 95);
            this.lblprueba.Name = "lblprueba";
            this.lblprueba.Size = new System.Drawing.Size(0, 13);
            this.lblprueba.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label1.Font = new System.Drawing.Font("Nirmala UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(84)))), ((int)(((byte)(89)))));
            this.label1.Location = new System.Drawing.Point(0, 564);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(200, 46);
            this.label1.TabIndex = 4;
            this.label1.Text = "Creado por Franco Balich";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.Location = new System.Drawing.Point(209, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(770, 54);
            this.panel2.TabIndex = 3;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(1)))), ((int)(((byte)(13)))));
            this.panel3.Controls.Add(this.pcbMin);
            this.panel3.Controls.Add(this.pcbClosed);
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1377, 50);
            this.panel3.TabIndex = 3;
            this.panel3.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Panel3_MouseMove);
            // 
            // pcbMin
            // 
            this.pcbMin.Image = global::Algoritmo_de_ruteo.Properties.Resources.minus;
            this.pcbMin.Location = new System.Drawing.Point(1309, 12);
            this.pcbMin.Name = "pcbMin";
            this.pcbMin.Size = new System.Drawing.Size(25, 25);
            this.pcbMin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pcbMin.TabIndex = 1;
            this.pcbMin.TabStop = false;
            this.pcbMin.Click += new System.EventHandler(this.PcbMin_Click);
            // 
            // pcbClosed
            // 
            this.pcbClosed.Image = global::Algoritmo_de_ruteo.Properties.Resources.close;
            this.pcbClosed.Location = new System.Drawing.Point(1340, 12);
            this.pcbClosed.Name = "pcbClosed";
            this.pcbClosed.Size = new System.Drawing.Size(25, 25);
            this.pcbClosed.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pcbClosed.TabIndex = 0;
            this.pcbClosed.TabStop = false;
            this.pcbClosed.Click += new System.EventHandler(this.PcbClosed_Click);
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.Font = new System.Drawing.Font("Nirmala UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(13)))), ((int)(((byte)(13)))));
            this.lblTitulo.Location = new System.Drawing.Point(9, 137);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(191, 30);
            this.lblTitulo.TabIndex = 4;
            this.lblTitulo.Text = "Sistema de routeo";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(147)))), ((int)(((byte)(183)))), ((int)(((byte)(191)))));
            this.panel4.Controls.Add(this.pictureBox1);
            this.panel4.Controls.Add(this.lblSubtitulo);
            this.panel4.Controls.Add(this.lblTitulo);
            this.panel4.Location = new System.Drawing.Point(0, 50);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(200, 198);
            this.panel4.TabIndex = 5;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Algoritmo_de_ruteo.Properties.Resources.logo;
            this.pictureBox1.Location = new System.Drawing.Point(16, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(163, 122);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // lblSubtitulo
            // 
            this.lblSubtitulo.AutoSize = true;
            this.lblSubtitulo.Font = new System.Drawing.Font("Nirmala UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSubtitulo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(84)))), ((int)(((byte)(89)))));
            this.lblSubtitulo.Location = new System.Drawing.Point(54, 167);
            this.lblSubtitulo.Name = "lblSubtitulo";
            this.lblSubtitulo.Size = new System.Drawing.Size(94, 25);
            this.lblSubtitulo.TabIndex = 6;
            this.lblSubtitulo.Text = "(Dijkstra)";
            // 
            // pnlPizarra
            // 
            this.pnlPizarra.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(217)))), ((int)(((byte)(217)))));
            this.pnlPizarra.Location = new System.Drawing.Point(200, 50);
            this.pnlPizarra.Name = "pnlPizarra";
            this.pnlPizarra.Size = new System.Drawing.Size(1177, 700);
            this.pnlPizarra.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Nirmala UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(13)))), ((int)(((byte)(13)))));
            this.label2.Location = new System.Drawing.Point(206, 753);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(212, 25);
            this.label2.TabIndex = 9;
            this.label2.Text = "Camino según Dijkstra";
            // 
            // GridDijkstra
            // 
            this.GridDijkstra.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.GridDijkstra.Location = new System.Drawing.Point(211, 783);
            this.GridDijkstra.Name = "GridDijkstra";
            this.GridDijkstra.Size = new System.Drawing.Size(680, 64);
            this.GridDijkstra.TabIndex = 8;
            // 
            // VistaGeneral
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.ClientSize = new System.Drawing.Size(1377, 856);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.GridDijkstra);
            this.Controls.Add(this.pnlPizarra);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "VistaGeneral";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pcbMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbClosed)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAddNewNode;
        private System.Windows.Forms.Button btnAddArista;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblSubtitulo;
        private System.Windows.Forms.Panel pnlPizarra;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pcbMin;
        private System.Windows.Forms.PictureBox pcbClosed;
        private GridPersonalizada GridDijkstra;
        private System.Windows.Forms.Label lblprueba;
        private System.Windows.Forms.Label lblAlgoritmo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnDijkstra;
        private System.Windows.Forms.Button button2;
    }
}

