﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Algoritmo_de_ruteo
{
    public partial class Vista_TablaEnrutamientos : Form
    {
        private readonly Nodo nodo;
        private readonly DataGridView gridRutas;
        private readonly DataGridView gridCamino;
        private readonly List<Enrutamiento> rutas = new List<Enrutamiento>();
        private readonly List<Camino> caminos = new List<Camino>();
        public Vista_TablaEnrutamientos(Nodo pNodo)
        {
            InitializeComponent();
            nodo = pNodo;
            gridRutas = gridEnrutamiento.Grid;
            gridCamino = gridDijktra.Grid;
        }

        private void Vista_TablaEnrutamientos_Load(object sender, EventArgs e)
        {
            lblName.Text += nodo.Id.ToString();

            foreach (Arista item in nodo.Aristas)
            {
                rutas.Add(new Enrutamiento()
                {
                    Puerto = item.PuertoEntrada,
                    Destino = item.NodoHijo.Id.ToString(),
                    Costo = item.Peso
                });
            }

            List<Nodo> caminoMasCorto = Grafo.CaminoMasCorto;
            if (caminoMasCorto.Count > 0)
            {
                if (nodo.SiguienteDijktra != null)
                {
                    if (caminoMasCorto.Exists(x => x.Id == nodo.Id))
                    {
                        Camino camino = new Camino();
                        if (nodo.Aristas.Exists(x => x.NodoHijo.Id == nodo.SiguienteDijktra.Id))
                        {
                            camino.Costo = nodo.Aristas.Find(x => x.NodoHijo.Id == nodo.SiguienteDijktra.Id).Peso;
                            camino.Destino = nodo.Aristas.Find(x => x.NodoHijo.Id == nodo.SiguienteDijktra.Id).NodoHijo.Id.ToString();
                            camino.Puerto = nodo.Aristas.Find(x => x.NodoHijo.Id == nodo.SiguienteDijktra.Id).PuertoEntrada;
                        }
                        else if (nodo.Aristas.Exists(x => x.NodoPadre.Id == nodo.SiguienteDijktra.Id))
                        {
                            camino.Costo = nodo.Aristas.Find(x => x.NodoPadre.Id == nodo.SiguienteDijktra.Id).Peso;
                            camino.Destino = nodo.Aristas.Find(x => x.NodoPadre.Id == nodo.SiguienteDijktra.Id).NodoPadre.Id.ToString();
                            camino.Puerto = nodo.Aristas.Find(x => x.NodoPadre.Id == nodo.SiguienteDijktra.Id).PuertoSalida;
                        }
                        camino.Final = caminoMasCorto.Last().Id.ToString();
                        caminos.Add(camino);
                        CargarGrid(gridCamino, caminos);
                    }
                    else
                    {
                        label1.Visible = true;
                        label1.Text = "Este nodo no pertenece al mejor camino.";
                    }
                }
            }
            else
            {
                label1.Visible = true;
                label1.Text = "Aun no se calculo el mejor camino.";
            }
            CargarGrid(gridRutas, rutas);
        }
        private Nodo BuscarNodoFinal(Nodo pNodo)
        {
            if (pNodo.SiguienteDijktra != null)
            {
                return BuscarNodoFinal(pNodo.SiguienteDijktra);
            }
            else
            {
                return pNodo;
            }
        }
        private void CargarGrid(DataGridView grid, object pLista)
        {
            grid.DataSource = null;
            grid.DataSource = pLista;
        }

        private class Enrutamiento
        {
            private string destino = "-";
            private string puerto = "-";
            private int peso;
            public Enrutamiento()
            {

            }
            public string Puerto { get => puerto; set => puerto = value; }
            public string Destino { get => destino; set => destino = value; }
            public int Costo { get => peso; set => peso = value; }
        }
        public class Camino
        {
            private string destino = "-";
            private string puerto = "-";
            private string final = "-";
            private int peso;
            public Camino()
            {

            }
            public string Puerto { get => puerto; set => puerto = value; }
            public int Costo { get => peso; set => peso = value; }
            public string Destino { get => destino; set => destino = value; }
            public string Final { get => final; set => final = value; }
        }
    }
}
