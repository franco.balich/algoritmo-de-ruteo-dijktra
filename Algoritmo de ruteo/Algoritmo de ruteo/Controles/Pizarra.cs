﻿using System.Windows.Forms;

namespace Algoritmo_de_ruteo
{
    public partial class VistaPizarra : Form
    {
        private Nodo nodoB;

        public VistaPizarra()
        {
            InitializeComponent();
        }
        public Nodo NodoA { get; set; }
        public Nodo NodoB
        {
            get => nodoB;
            set
            {
                nodoB = value;
                _ = new Arista(NodoA, nodoB, this);
                NodoA = null;
                nodoB = null;
            }
        }
        public int State { get; set; } = 0;

        public void AddArista()
        {
            State = 1;
            NodoA = null;
            nodoB = null;
        }

    }
}
