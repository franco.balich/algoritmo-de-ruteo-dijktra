﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace Algoritmo_de_ruteo
{
    public class OvalPictureBox : PictureBox
    {
        public OvalPictureBox() { }
        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            using (GraphicsPath gp = new GraphicsPath())
            {
                gp.AddEllipse(new Rectangle(0, 0, Width - 1, Height - 1));
                Region = new Region(gp);
            }
        }
    }
}
