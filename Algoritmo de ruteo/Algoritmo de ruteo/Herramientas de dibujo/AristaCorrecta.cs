﻿using System.Drawing;

namespace Algoritmo_de_ruteo
{
    internal class AristaCorrecta : IDibujable
    {
        private readonly VistaPizarra pantalla;

        public AristaCorrecta(Nodo pNodoA, Nodo pNodoB, VistaPizarra pForm)
        {
            if (pNodoA.Id != pNodoB.Id)
            {
                NodoPadre = pNodoA;
                NodoHijo = pNodoB;
                pantalla = pForm;

                Suscribir(this);
                Redibujar();
            }
        }
        public Nodo NodoPadre { get; }

        public Nodo NodoHijo { get; }


        public void Redibujar()
        {
            PointF A = new PointF(NodoPadre.X, NodoPadre.Y);
            PointF B = new PointF(NodoHijo.X, NodoHijo.Y);

            Pen linea = new Pen(Color.Green, 7);

            Graphics e = pantalla.CreateGraphics();
            e.DrawLine(linea, A, B);
        }

        public void Suscribir(IDibujable arista)
        {
            Graficador.Suscribir(arista);
        }

        public void Desuscribir()
        {
            Graficador.Desuscribir(this);
        }
    }
}
