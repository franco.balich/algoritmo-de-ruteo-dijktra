﻿using Microsoft.VisualBasic;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace Algoritmo_de_ruteo
{
    public class Arista : IDibujable
    {
        private readonly OvalPictureBox img;
        private readonly Label lbl;
        private readonly Label lblPuertoEntrada;
        private readonly Label lblPuertoSalida;
        private readonly VistaPizarra pantalla;
        private const string puertos = "-ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        public Arista(Nodo pNodoA, Nodo pNodoB, VistaPizarra pForm)
        {

            if (pNodoA.Id != pNodoB.Id)
            {
                NodoPadre = pNodoA;
                NodoHijo = pNodoB;
                pantalla = pForm;

                PointF A = new PointF(NodoPadre.X, NodoPadre.Y);
                PointF B = new PointF(NodoHijo.X, NodoHijo.Y);
                PointF C = new PointF((A.X + ((B.X - A.X) / 2)) + 5, (A.Y + ((B.Y - A.Y) / 2)) - 20);
                PointF D = new PointF((A.X + ((B.X - A.X) / 4)), (A.Y + ((B.Y - A.Y) / 4)));
                PointF E = new PointF((A.X + ((B.X - A.X) / 4)), (A.Y + ((B.Y - A.Y) / 4)));

                img = new OvalPictureBox();
                img.Location = new Point((int)C.X - (img.Width / 2), (int)C.Y - (img.Height / 2));
                img.Size = new Size(30, 30);
                img.SizeMode = PictureBoxSizeMode.StretchImage;
                img.Image = Properties.Resources.circulo;
                pantalla.Controls.Add(img);
                img.Visible = true;
                img.BringToFront();

                lbl = new Label
                {
                    Parent = img,
                    Left = (int)C.X - 6,
                    Top = (int)C.Y - 10,
                    AutoSize = false,
                    Text = Peso.ToString(),
                    Width = 12,
                    BackColor = Color.Black,
                    TextAlign = ContentAlignment.MiddleCenter,
                    Font = new Font("Arial", 12, FontStyle.Bold),
                    ForeColor = Color.White
                };

                lbl.Click += new EventHandler(Element_Click);
                pantalla.Controls.Add(lbl);
                lbl.BringToFront();
                pNodoA.AddArista(this);
                pNodoB.AddArista(this);
                Suscribir(this);


                PuertoEntrada = puertos.Substring(pNodoA.Aristas.Count, 1);
                PuertoSalida = puertos.Substring(pNodoB.Aristas.Count, 1);

                lblPuertoEntrada = new Label
                {
                    Left = (int)A.X - 40,
                    Top = (int)A.Y - 40,
                    AutoSize = false,
                    Text = PuertoEntrada,
                    Width = 12,
                    TextAlign = ContentAlignment.MiddleCenter,
                    Font = new Font("Arial", 12, FontStyle.Bold),
                    ForeColor = Color.Black
                };

                lblPuertoSalida = new Label
                {
                    Left = (int)B.X + 40,
                    Top = (int)B.Y + 40,
                    AutoSize = false,
                    Text = PuertoSalida,
                    Width = 12,
                    TextAlign = ContentAlignment.MiddleCenter,
                    Font = new Font("Arial", 12, FontStyle.Bold),
                    ForeColor = Color.Black
                };
                pantalla.Controls.Add(lblPuertoEntrada);
                pantalla.Controls.Add(lblPuertoSalida);
                Redibujar();
                Grafo.AdicionaArista(NodoPadre, NodoHijo);
            }
            else
            {
                throw new Exception("No se crear un enlace que conecte a una computadora consigo misma.");
            }
        }

        private void Element_Click(object sender, EventArgs e)
        {
            try
            {
                Label lbl = sender as Label;
                int aux = Convert.ToInt32(Interaction.InputBox("Ingrese el nuevo peso de la arista"));
                if (aux > 0 && aux < 10)
                {
                    Peso = aux;
                    lbl.Text = Peso.ToString();
                    Grafo.ActualizarAdyacencias(NodoPadre, NodoHijo, Peso);
                    Grafo.ActualizarAdyacencias(NodoHijo, NodoPadre, Peso);
                }
                else
                {
                    MessageBox.Show("El peso solicitado esta fuera del rango posible \r\n(Rango posible: 1-9).", "", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public int Peso { get; set; } = 1;

        public Nodo NodoPadre { get; }

        public Nodo NodoHijo { get; }

        public string PuertoEntrada { get; set; }
        public string PuertoSalida { get; set; }

        internal void Borrar()
        {
            img.Dispose();
            lbl.Dispose();
        }

        public void Redibujar()
        {
            PointF A = new PointF(NodoPadre.X, NodoPadre.Y);
            PointF B = new PointF(NodoHijo.X, NodoHijo.Y);
            PointF C = new PointF((A.X + ((B.X - A.X) / 2)), (A.Y + ((B.Y - A.Y) / 2)));

            PointF D = new PointF((A.X + (((B.X - A.X) / 2)) / 2), (A.Y + (((B.Y - A.Y) / 2)) / 2));
            PointF E = new PointF((B.X - (((B.X - A.X) / 2)) / 2), (B.Y - (((B.Y - A.Y) / 2)) / 2));
            lbl.Left = (int)C.X - 6;
            lbl.Top = (int)C.Y - 10;

            lblPuertoEntrada.Left = (int)D.X;
            lblPuertoEntrada.Top = (int)D.Y;

            lblPuertoSalida.Left = (int)E.X;
            lblPuertoSalida.Top = (int)E.Y;

            img.Location = new System.Drawing.Point((int)C.X - (img.Width / 2), (int)C.Y - (img.Height / 2));
            Pen linea = new Pen(Color.Red, 3);

            Graphics e = pantalla.CreateGraphics();

            e.DrawLine(linea, A, B);
        }
        public void Suscribir(IDibujable arista)
        {
            Graficador.Suscribir(arista);
        }
    }
}
