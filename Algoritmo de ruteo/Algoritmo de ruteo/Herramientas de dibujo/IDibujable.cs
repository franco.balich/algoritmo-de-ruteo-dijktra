﻿namespace Algoritmo_de_ruteo
{
    public interface IDibujable
    {
        void Redibujar();
        void Suscribir(IDibujable arista);
    }
}
