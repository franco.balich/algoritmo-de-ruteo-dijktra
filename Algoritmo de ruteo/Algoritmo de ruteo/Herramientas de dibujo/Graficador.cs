﻿using System.Collections.Generic;
using System.Drawing;

namespace Algoritmo_de_ruteo
{
    public static class Graficador
    {
        private static readonly List<IDibujable> aristas = new List<IDibujable>();
        public static void Redibujar()
        {

            if (aristas.Count > 0)
            {
                foreach (IDibujable arista in aristas)
                {
                    arista.Redibujar();
                }
            }
        }
        public static void LimpiarPantalla(VistaPizarra pPantalla)
        {
            Graphics e = pPantalla.CreateGraphics();
            e.Clear(Color.FromArgb(217, 217, 217));
        }
        public static void Suscribir(IDibujable arista)
        {
            if (arista != null)
            {
                aristas.Add(arista);
            }
        }
        public static void Desuscribir(IDibujable arista)
        {
            if (arista != null)
            {
                aristas.Remove(arista);
            }
        }
    }
}
