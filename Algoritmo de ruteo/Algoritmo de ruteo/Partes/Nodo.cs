﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Algoritmo_de_ruteo
{
    public class Nodo
    {
        private readonly List<Nodo> nodoAdyasente;
        private OvalPictureBox img;
        private Label lbl;

        private readonly VistaPizarra pizarra;

        public Nodo(decimal pPeso, VistaGeneral pForm)
        {
            Id = GeneradorCodigos.Code;
            Peso = pPeso;
            nodoAdyasente = new List<Nodo>();
            pizarra = pForm.Pizarra;
            CargarControles();
        }
        public Nodo(VistaGeneral pForm)
        {
            Id = GeneradorCodigos.Code;
            Peso = 1;
            nodoAdyasente = new List<Nodo>();
            pizarra = pForm.Pizarra;
            CargarControles();

            if (Id == 0)
            {
                img.Left = 1000;
                img.Top = 1000;
                lbl.Top = 1000;
                lbl.Left = 1000;
            }
        }
        public void CargarControles()
        {
            Graphics e = pizarra.CreateGraphics();
            img = new OvalPictureBox
            {
                Location = new System.Drawing.Point(50, 100),
                Size = new System.Drawing.Size(80, 80),
                SizeMode = PictureBoxSizeMode.StretchImage,
                Image = Properties.Resources.pc
            };
            pizarra.Controls.Add(img);
            img.Visible = true;
            img.BringToFront();

            img.MouseMove += new System.Windows.Forms.MouseEventHandler(Img_MouseMove);
            img.Click += new System.EventHandler(Img_Click);


            lbl = new Label
            {
                Parent = img,
                Left = img.Left + (img.Width / 2) - 10,
                Top = img.Top + img.Height - 25,
                AutoSize = false,
                BackColor = Color.White,
                Width = 20,
                TextAlign = ContentAlignment.MiddleCenter,
                Text = Id.ToString()
            };
            pizarra.Controls.Add(lbl);
            lbl.BringToFront();
        }
        private void Img_Click(object sender, EventArgs e)
        {
            try
            {
                if (Grafo.EstadoTarea == 0)
                {
                    if (pizarra.State >= 1 && pizarra.State < 3)
                    {
                        if (pizarra.NodoA == null)
                        {
                            pizarra.NodoA = this;
                            pizarra.State = 2;
                        }
                        else if (pizarra.NodoA != null && pizarra.NodoB == null && pizarra.State == 2)
                        {
                            pizarra.NodoB = this;
                            pizarra.State = 3;
                        }
                    }
                }
                else if (Grafo.EstadoTarea == 1) //Borrar
                {
                    img.Dispose();
                    lbl.Dispose();
                    foreach (Arista item in Aristas)
                    {
                        item.Borrar();
                    }
                    Grafo.EstadoTarea = 0;
                }
                else if (Grafo.EstadoTarea == 2) //Ver tabla de enrutamiento
                {
                    Vista_TablaEnrutamientos frm = new Vista_TablaEnrutamientos(this);
                    frm.ShowDialog();
                    Grafo.EstadoTarea = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void Img_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left)
            {
                X1 = e.X;
                Y1 = e.Y;
            }
            else
            {
                img.Left += (e.X - X1);
                img.Top += (e.Y - Y1);
                lbl.Left = img.Left + (img.Width / 2) - 5;
                lbl.Top = img.Top + img.Height - 23;

                Graphics g = pizarra.CreateGraphics();
                g.Clear(pizarra.BackColor);
                Graficador.Redibujar();
            }
        }
        public void AddArista(Arista pArista)
        {
            if (pArista != null)
            {
                Aristas.Add(pArista);
            }
        }
        public Arista BuscarArista(Nodo pNodoHijo)
        {
            if (pNodoHijo != null)
            {
                return Aristas.Find(x => (x.NodoHijo.Id == pNodoHijo.Id) || (((x.NodoPadre.Id == pNodoHijo.Id)) && (x.NodoPadre.Id != Id)));
            }
            else
            {
                return null;
            }
        }
        public void AgregarNodoAdyasente(Nodo pNodo)
        {
            nodoAdyasente.Add(pNodo);
        }
        public int Id { get; set; }

        public int X => img.Left + (img.Width / 2);
        public int Y => img.Top + (img.Height / 2);
        public List<Arista> Aristas { get; set; } = new List<Arista>();
        public Nodo SiguienteDijktra { get; set; }
        public int X1 { get; set; } = 0;
        public int Y1 { get; set; } = 0;

        public decimal Peso { get; }

        public override string ToString()
        {
            return $"ID: {Id} Peso: {Peso}";
        }
    }
}
