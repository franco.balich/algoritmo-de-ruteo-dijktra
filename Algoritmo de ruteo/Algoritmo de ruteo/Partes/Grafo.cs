﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Algoritmo_de_ruteo
{
    public sealed class Grafo
    {
        private static decimal[,] mAdyacencias;
        private static int[] indegree;
        private static int estadoTarea = 0;
        private static List<Nodo> nodos;
        private static readonly List<AristaCorrecta> caminoresaltado = new List<AristaCorrecta>();
        private const int LARGO = 21;
        private static readonly List<Nodo> caminoMasCorto = new List<Nodo>();

        private static VistaPizarra pantalla;
        public Grafo(VistaPizarra pPantalla)
        {
            pantalla = pPantalla;
            nodos = new List<Nodo>();
            Gestor_Dijktra.GrafoPrincipal = this;
            if (mAdyacencias == null && indegree == null)
            {
                mAdyacencias = new decimal[LARGO, LARGO];
                indegree = new int[LARGO + 1];
            }
        }
        public int CantidadNodos => nodos.Count();
        public static int EstadoTarea { get => estadoTarea; set => estadoTarea = value; }

        public static void CargarCamino(Nodo pCamino)
        {
            if (pCamino != null)
            {
                caminoMasCorto.Add(pCamino);
            }
        }
        public static void ResetCamino()
        {
            caminoMasCorto.Clear();

        }
        public static VistaPizarra Pantalla => pantalla;

        public static List<Nodo> CaminoMasCorto => caminoMasCorto;

        public void AgregarNodo(VistaGeneral pForm)
        {
            if (nodos.Count < LARGO)
            {
                Nodo nuevo = new Nodo(pForm);
                if (!nodos.Exists(x => x.Id == nuevo.Id))
                {
                    nodos.Add(nuevo);
                }
            }
            else
            {
                throw new Exception("Supero el limite de computadoras que se puede asignar a la red");
            }
        }
        public static Nodo BuscarNodo(int buscado)
        {
            if (buscado >= 0)
            {
                return nodos.Find(x => x.Id == buscado);
            }
            else
            {
                return null;
            }

        }
        public static void AdicionaArista(Nodo pNodoInicio, Nodo pNodoFinal)
        {
            int inicio = pNodoInicio.Id;
            int fin = pNodoFinal.Id;

            if (mAdyacencias == null && indegree == null)
            {
                mAdyacencias = new decimal[LARGO, LARGO];
                indegree = new int[LARGO + 1];
            }

            Nodo nodoPadre = BuscarNodo(inicio);
            Nodo nodoHijo = BuscarNodo(fin);
            nodoPadre.AgregarNodoAdyasente(nodoHijo);

            mAdyacencias[inicio, fin] = nodoPadre.BuscarArista(nodoHijo).Peso;
            mAdyacencias[fin, inicio] = nodoHijo.BuscarArista(nodoPadre).Peso;
        }
        public static void ActualizarAdyacencias(Nodo pNodoPadre, Nodo pNodoHijo, int pPeso)
        {
            if (pNodoPadre != null && pNodoPadre != null)
            {
                int inicio = pNodoPadre.Id;
                int fin = pNodoHijo.Id;
                mAdyacencias[inicio, fin] = pPeso;
            }
        }
        public string MuestraAdyacencia()
        {
            if (mAdyacencias != null)
            {
                string cadena = "  |";
                int n;
                int m;

                for (n = 0; n < nodos.Count(); n++)
                {
                    cadena += $" {n }";
                }
                cadena += $"\n\r";
                for (n = 0; n < nodos.Count(); n++)
                {
                    cadena += $"{n }|";
                    for (m = 0; m < nodos.Count(); m++)
                    {
                        cadena += $" {mAdyacencias[n, m]}";
                    }
                    cadena += $"\n\r";
                }
                return cadena;
            }
            else
            {
                throw new Exception("No hay alguna arista creada");
            }
        }

        public decimal ObtenAyacencia(int pfila, int pColumna)
        {
            return mAdyacencias[pfila, pColumna];
        }


        internal void VerTablaDeEnrutamiento()
        {
            EstadoTarea = 2;
        }

        internal static void DibujarCaminoCritico()
        {
            if (CaminoMasCorto.Count > 0)
            {
                foreach (AristaCorrecta item in caminoresaltado)
                {
                    item.Desuscribir();
                }
                caminoresaltado.Clear();
                for (int i = 0; i < CaminoMasCorto.Count; i++)
                {
                    Nodo camino = CaminoMasCorto.ElementAt(i);
                    if (i < CaminoMasCorto.Count - 1)
                    {
                        Nodo sig = CaminoMasCorto.ElementAt(i + 1);
                        AristaCorrecta linea = new AristaCorrecta(camino, sig, Pantalla);
                        caminoresaltado.Add(linea);
                    }
                }
            }

        }
    }
}
