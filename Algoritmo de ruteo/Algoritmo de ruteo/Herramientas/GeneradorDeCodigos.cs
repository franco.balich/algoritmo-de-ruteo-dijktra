﻿namespace Algoritmo_de_ruteo
{
    public class GeneradorCodigos
    {

        private static int code;
        private GeneradorCodigos()
        {
        }

        public static int Code
        {
            get
            {
                int aux = code;
                code++;
                return aux;
            }
        }
    }
}
